# Overview
I made something slighty different than intended. Instead of an messaging app it turned to an open chat-room where you could review any previous message, and even search.
In this application you can: send messages, search for messages and make an account. Every account have its own username.

# Testing
First you need to run 'flask run', then you can connect to http://localhost:5000/
Begin by registrating for an account (or several), then you can begin typing messages, reload/logout to see the accumalated text and try to search for messages using the search function.

# Technical details
Every account is unique and is stored in a database. The messages are also stored in a database and gets pulled everytime you reload the site.

# Questions
People who dont like a completly open chat with no restrictions might attack the site. The attacker might send a really big message (ddos attack), modify html code or maybe try get hold of everyones passwords (the database containing passwords is not hashed). The attacker might completly take down the availability by crashing the server with a ddos. The site is not very confidential since every message is visible, so its not much the attacker can do. The main attack vector for this program is httml modification, it is very vulnerable. We should hash the passwords, make the httml more secure and make ddos attack protection. I have whitelisted charaters and made prepeared statements so it is impossible to do sql-injections. I have also reduced the amount of information given to the user of whats happening backend. I know its not good enough now, but I have way to much time into the little I have managed to do.
